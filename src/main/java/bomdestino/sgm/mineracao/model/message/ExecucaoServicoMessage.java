package bomdestino.sgm.mineracao.model.message;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class ExecucaoServicoMessage {

    private Long idServico;
    private String nomeServico;
    private BigDecimal valor;
    private Integer prazoAtendimento;
    private String unidadeMedidaPrazo;

    private Long idUsuarioSolicitacao;
    private Integer cep;
    private String endereco;
    private String observacao;

    private String responsavel;
    private String contato;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime dataHoraSolicitacao;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime dataHoraAprovacao;

    public Long getIdServico() {
        return idServico;
    }

    public ExecucaoServicoMessage setIdServico(Long idServico) {
        this.idServico = idServico;
        return this;
    }

    public String getNomeServico() {
        return nomeServico;
    }

    public ExecucaoServicoMessage setNomeServico(String nomeServico) {
        this.nomeServico = nomeServico;
        return this;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public ExecucaoServicoMessage setValor(BigDecimal valor) {
        this.valor = valor;
        return this;
    }

    public Integer getPrazoAtendimento() {
        return prazoAtendimento;
    }

    public ExecucaoServicoMessage setPrazoAtendimento(Integer prazoAtendimento) {
        this.prazoAtendimento = prazoAtendimento;
        return this;
    }

    public String getUnidadeMedidaPrazo() {
        return unidadeMedidaPrazo;
    }

    public ExecucaoServicoMessage setUnidadeMedidaPrazo(String unidadeMedidaPrazo) {
        this.unidadeMedidaPrazo = unidadeMedidaPrazo;
        return this;
    }

    public Long getIdUsuarioSolicitacao() {
        return idUsuarioSolicitacao;
    }

    public ExecucaoServicoMessage setIdUsuarioSolicitacao(Long idUsuarioSolicitacao) {
        this.idUsuarioSolicitacao = idUsuarioSolicitacao;
        return this;
    }

    public Integer getCep() {
        return cep;
    }

    public ExecucaoServicoMessage setCep(Integer cep) {
        this.cep = cep;
        return this;
    }

    public String getEndereco() {
        return endereco;
    }

    public ExecucaoServicoMessage setEndereco(String endereco) {
        this.endereco = endereco;
        return this;
    }

    public String getObservacao() {
        return observacao;
    }

    public ExecucaoServicoMessage setObservacao(String observacao) {
        this.observacao = observacao;
        return this;
    }

    public String getResponsavel() {
        return responsavel;
    }

    public ExecucaoServicoMessage setResponsavel(String responsavel) {
        this.responsavel = responsavel;
        return this;
    }

    public String getContato() {
        return contato;
    }

    public ExecucaoServicoMessage setContato(String contato) {
        this.contato = contato;
        return this;
    }

    public LocalDateTime getDataHoraSolicitacao() {
        return dataHoraSolicitacao;
    }

    public ExecucaoServicoMessage setDataHoraSolicitacao(LocalDateTime dataHoraSolicitacao) {
        this.dataHoraSolicitacao = dataHoraSolicitacao;
        return this;
    }

    public LocalDateTime getDataHoraAprovacao() {
        return dataHoraAprovacao;
    }

    public ExecucaoServicoMessage setDataHoraAprovacao(LocalDateTime dataHoraAprovacao) {
        this.dataHoraAprovacao = dataHoraAprovacao;
        return this;
    }
}
