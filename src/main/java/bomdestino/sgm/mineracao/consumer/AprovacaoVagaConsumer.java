package bomdestino.sgm.mineracao.consumer;

import bomdestino.sgm.mineracao.model.message.AprovacaoVagaMessage;
import bomdestino.sgm.mineracao.service.EducacaoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AprovacaoVagaConsumer {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private EducacaoService service;

    @RabbitListener(queues = "minerador-dados-educacao")
    public void listenGroupFoo(AprovacaoVagaMessage message) {
        logger.info("Obtendo mensagem de aprovacao de vaga para registro estatistico");
        service.registrarAprovacaoVaga(message);
    }
}
