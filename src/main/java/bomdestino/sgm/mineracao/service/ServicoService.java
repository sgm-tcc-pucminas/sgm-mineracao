package bomdestino.sgm.mineracao.service;

import bomdestino.sgm.mineracao.model.message.ExecucaoServicoMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class ServicoService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public void registrarExecucaoServico(ExecucaoServicoMessage message) {
        StringBuilder mensagem = new StringBuilder();
        mensagem.append("\n\n [*** SERVICO ***] ----------------------------------------- [*** MINERADOR INICIO ***]");
        mensagem.append("\n\n idServico: " + message.getIdServico());
        mensagem.append("\n\n idUsuarioSolicitacao: " + message.getIdUsuarioSolicitacao());
        mensagem.append("\n\n responsável: " + message.getResponsavel());
        mensagem.append("\n\n cep: " + message.getCep());
        mensagem.append("\n\n data hora solicitacao: " + message.getDataHoraSolicitacao());
        mensagem.append("\n\n data hora aprovacao: " + message.getDataHoraAprovacao());
        mensagem.append("\n\n[*** SERVICO ***] ----------------------------------------- [*** MINERADOR FIM ***]");

        logger.info(mensagem.toString());
    }
}