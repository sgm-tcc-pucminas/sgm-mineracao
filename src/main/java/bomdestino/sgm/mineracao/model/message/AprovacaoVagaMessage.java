package bomdestino.sgm.mineracao.model.message;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import java.time.LocalDateTime;

public class AprovacaoVagaMessage {

    private Long idEscola;
    private String nome;
    private String email;
    private String telefone;
    private Long idUsuarioAprovacao;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime dataHoraAprovacao;

    public Long getIdEscola() {
        return idEscola;
    }

    public AprovacaoVagaMessage setIdEscola(Long idEscola) {
        this.idEscola = idEscola;
        return this;
    }

    public String getNome() {
        return nome;
    }

    public AprovacaoVagaMessage setNome(String nome) {
        this.nome = nome;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public AprovacaoVagaMessage setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getTelefone() {
        return telefone;
    }

    public AprovacaoVagaMessage setTelefone(String telefone) {
        this.telefone = telefone;
        return this;
    }

    public Long getIdUsuarioAprovacao() {
        return idUsuarioAprovacao;
    }

    public AprovacaoVagaMessage setIdUsuarioAprovacao(Long idUsuarioAprovacao) {
        this.idUsuarioAprovacao = idUsuarioAprovacao;
        return this;
    }

    public LocalDateTime getDataHoraAprovacao() {
        return dataHoraAprovacao;
    }

    public AprovacaoVagaMessage setDataHoraAprovacao(LocalDateTime dataHoraAprovacao) {
        this.dataHoraAprovacao = dataHoraAprovacao;
        return this;
    }
}
