package bomdestino.sgm.mineracao.service;

import bomdestino.sgm.mineracao.model.message.AprovacaoVagaMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class EducacaoService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public void registrarAprovacaoVaga(AprovacaoVagaMessage message) {
        StringBuilder mensagem = new StringBuilder();
        mensagem.append("\n\n [*** EDUCACAO ***] ----------------------------------------- [*** MINERADOR INICIO ***]");
        mensagem.append("\n\n idEscola: " + message.getIdEscola());
        mensagem.append("\n\n usuário solicitação: " + message.getNome());
        mensagem.append("\n\n telefone: " + message.getTelefone());
        mensagem.append("\n\n email: " + message.getEmail());
        mensagem.append("\n\n data hora aprovacao: " + message.getDataHoraAprovacao());
        mensagem.append("\n\n[*** EDUCACAO ***] ----------------------------------------- [*** MINERADOR FIM ***]");

        logger.info(mensagem.toString());
    }
}