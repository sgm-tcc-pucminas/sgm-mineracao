package bomdestino.sgm.mineracao.config.mensageria;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableRabbit
public class RabbitMQConfig {

    @Bean
    public MessageConverter jsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public Queue mineradorDadosEducacaoQueue() {
        return new Queue("minerador-dados-educacao", true, false, false);
    }

    @Bean
    FanoutExchange mineradorDadosEducacaoExchange() {
        return new FanoutExchange("minerador-dados-educacao");
    }

    @Bean
    Binding mineradorDadosEducacaoBinding(Queue mineradorDadosEducacaoQueue,
                                          FanoutExchange mineradorDadosEducacaoExchange) {

        return BindingBuilder.bind(mineradorDadosEducacaoQueue).to(mineradorDadosEducacaoExchange);
    }

    @Bean
    public Queue mineradorDadosExecucaoServicoQueue() {
        return new Queue("minerador-dados-execucao-servico", true, false, false);
    }

    @Bean
    FanoutExchange mineradorDadosExecucaoServicoExchange() {
        return new FanoutExchange("minerador-dados-execucao-servico");
    }

    @Bean
    Binding mineradorDadosExecucaoServicoBinding(Queue mineradorDadosExecucaoServicoQueue,
                                                 FanoutExchange mineradorDadosExecucaoServicoExchange) {

        return BindingBuilder.bind(mineradorDadosExecucaoServicoQueue).to(mineradorDadosExecucaoServicoExchange);
    }
}
