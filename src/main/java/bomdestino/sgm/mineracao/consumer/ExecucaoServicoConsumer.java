package bomdestino.sgm.mineracao.consumer;

import bomdestino.sgm.mineracao.model.message.ExecucaoServicoMessage;
import bomdestino.sgm.mineracao.service.ServicoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ExecucaoServicoConsumer {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ServicoService service;

    @RabbitListener(queues = "minerador-dados-execucao-servico")
    public void listenGroupFoo(ExecucaoServicoMessage message) {
        logger.info("Obtendo mensagem de execucacao de servico para registro estatistico");
        service.registrarExecucaoServico(message);
    }
}
